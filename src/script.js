$(document).ready(() => {

    // add a request to fetch data from server
    const data = {
        users: [
            {
                first_name: 'yosi',
                last_name: 'shwartz',
                created: '29/2/2020',
                email: 'abc@gmail.com',
                status: 'active',
                demi: false
            },
            {
                first_name: 'nadav',
                last_name: 'nobel',
                created: '22/2/2020',
                email: 'abd@gmail.com',
                status: 'inactive',
                demi: true
            }
        ],
        current_user: null,
        modal_type: null
    };

    removeModal = () => {
        $('.content').css('display', 'none');
        $('#modal').css('display', 'none');
    };

    const renderList = () => {
        for (let i = 0; i < data.users.length; i++) {
            const user = `<tr user_ref="${i}"> <td class="icon user"></td> <td>${data.users[i].first_name} ${data.users[i].last_name}</td> <td>${data.users[i].created}</td> <td> <button class="status ${data.users[i].status}" disabled>${data.users[i].status}</button> </td> <td>${data.users[i].email}</td> <td> <div class="icon small edit"></div> <div class="icon small more"></div> <div class="icon small delete"></div> </td> </tr>`
            $('table > tbody').append(user);
        }
    };

    renderList();

    $('.icon.small').on('click', el => {
        const kind = el.target.classList[2];
        data.current_user = el.target.parentElement.parentElement.getAttribute('user_ref');
        const modal = $('#modal');
        let content = modal.find('.content.' + kind);
        if(kind === 'edit'){
            const inputs = content.find('input');
            for(let i = 0; i < inputs.length; i++){
                inputs[i].value = data.users[data.current_user][inputs[i].name]
            }
        }
        content.css('display', 'block');
        modal.css('display', 'block');
    });

    $('button.cancel').on('click', removeModal);

    $('button.confirm').on('click', el => {
        const kind = el.target.classList[2];
        if (kind === 'edit'){
            const content = $('#modal .content:visible');
            const inputs = content.find('input');
            for(let i = 0; i < inputs.length; i++){
                data.users[data.current_user][inputs[i].name] = inputs[i].value
            }
        }
        else if(kind === 'add'){

        }
        else{
            data.users.slice(data.current_user);
            $(`tr[user_ref="${data.current_user}"]`).remove();
        }
        removeModal();
    });

});